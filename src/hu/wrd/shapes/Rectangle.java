package hu.wrd.shapes;

public class Rectangle implements Shape {

    private int x;
    private int y;


    private double area;

    Rectangle(int x, int y) throws InvalidSizeException {
        if(x <= 0 || y <= 0){
            throw new InvalidSizeException("Érvénytelen méret, nem lehet 0 vagy negatív!");
        }
        this.x = x;
        this.y = y;
        this.area = x * y;
    }

    @Override
    public double getArea() {
        return area;
    }
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
    @Override
    public boolean equals(Shape shape) {
        if (shape == null || (shape.getClass() != Rectangle.class && shape.getClass() != Square.class)){
            System.out.println("asd");
            return false;
        }
        if (this == shape) return true;
        if(shape.getClass() == Rectangle.class){
            if((this.x == ((Rectangle) shape).getX() && this.y == ((Rectangle) shape).getY()) ||
                this.x == ((Rectangle) shape).getY() && this.y == ((Rectangle) shape).getX()){
                return true;
            }
        } else if (shape.getClass() == Square.class) {
            if(this.x == this.y && this.x == ((Square) shape).getX()){
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString(){
        return "Téglalap x oldala: " + x + ", y oldala: " + y + ", területe: " + area;
    }
}
