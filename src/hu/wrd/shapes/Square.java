package hu.wrd.shapes;

public class Square implements Shape {

    private int x;
    private double area;

    Square(int x) throws InvalidSizeException {
        if(x <= 0){
            throw new InvalidSizeException("Érvénytelen méret, nem lehet 0 vagy negatív!");
        }
        this.x = x;
        this.area = x * x;
    }

    @Override
    public double getArea() {
        return area;
    }
    public int getX() {
        return x;
    }

    @Override
    public boolean equals(Shape shape) {
        if (shape == null || (shape.getClass() != Rectangle.class && shape.getClass() != Square.class)) return false;
        if (this == shape) return true;
        if(shape.getClass() == Square.class){
            if(this.x == ((Square) shape).getX()){
                return true;
            }
        } else if (shape.getClass() == Rectangle.class) {
            if(this.x == ((Rectangle) shape).getX() && this.x == ((Rectangle) shape).getY()){
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString(){
        return "Négyzet oldala: " + x + ", területe: " + area;
    }
}
