package hu.wrd.shapes;

class InvalidSizeException extends Exception {
    public InvalidSizeException(String s)
    {
        // Call constructor of parent Exception
        super(s);
    }
}

public interface Shape {

    /**
     *
     * @return Area of shape
     */
    public double getArea();

    /**
     * Checks not only if the shape are the same object, but if they look the same, for example Rectange(5,2) and Rectangle(2,5) are equal
     * @param shape the shape to compare to
     * @return true if equal, false otherwise
     */
    boolean equals(Shape shape);
}
