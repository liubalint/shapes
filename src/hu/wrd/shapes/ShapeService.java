package hu.wrd.shapes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class ShapeService {
    private ArrayList<Shape> shapeList = new ArrayList<>();

    ShapeService(){};

    /**
     * Adds zero or more shapes to shapeList, if none of them are already in the list
     * @param shapes the zero or more shapes to add
     */
    public void addShapes(Shape ... shapes) {
        if(shapeList.stream().anyMatch(oldShape -> Arrays.stream(shapes).anyMatch(newShape -> oldShape.equals(newShape)))){
            System.out.println("Mivel volt megadva olyan shape, ami már benne volt a listában, ezért egy új shape sem lett hozzáadva.");
            return;
        }
        for (Shape shape : shapes) {
            shapeList.add(shape);
        }
    }

    /**
     * Prints out to the console all the stored shapes by area ascending
     */
    public void printShapesOrderByAreaAsc() {
        Collections.sort(shapeList, Comparator.comparingDouble(Shape::getArea));
        shapeList.forEach(shape -> System.out.println(shape.toString()));
        System.out.println("printShapesOrderByAreaAsc() called");
    }

    /**
     * Prints out to the console all the stored shapes by area descending
     */
    public void printShapesOrderByAreaDesc() {
        Collections.sort(shapeList, Comparator.comparingDouble(Shape::getArea).reversed());
        shapeList.forEach(shape -> System.out.println(shape.toString()));
        System.out.println("printShapesOrderByAreaDesc() called");
    }

}
