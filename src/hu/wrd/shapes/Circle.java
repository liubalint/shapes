package hu.wrd.shapes;

public class Circle implements Shape {


    private final int r;
    private double area;
    Circle(int radius) throws InvalidSizeException {
        if(radius <= 0){
            throw new InvalidSizeException("Érvénytelen méret, nem lehet 0 vagy negatív!");
        }
        this.r = radius;
        this.area = Math.PI * r * r;
    }

    @Override
    public double getArea() {
        return area;
    }

    public int getR() {
        return r;
    }
    @Override
    public boolean equals(Shape shape) {
        if (shape == null || shape.getClass() != Circle.class) return false;
        if (this == shape || this.r == ((Circle) shape).getR()) return true;
        return false;
    }
    @Override
    public String toString(){
        return "Kör rádiusza: " + r + ", területe: " + area;
    }
}
